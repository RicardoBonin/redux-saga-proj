const INITTIAL_STATE ={
  data: [],
  isFeching: false,
  error: false
}

const ip = (state = INITTIAL_STATE, action) => {
  console.log(action)
  if(action.type === 'LOAD_DATA_REQUEST'){
    return {
      isFeching: true,
      data: [],
      error: false
    }
  }
  if(action.type === 'LOAD_DATA_SUCCESS'){
    return {
      isFeching: true,
      data: action.data,
      error: false
    }
  }
  if(action.type === 'LOAD_DATA_FAILURE'){
    return {
      isFeching: false,
      data: [],
      error: true
    }
  }
  return state
}
export default ip