const INITTIAL_STATE ={
    data: [],
    isFeching: false,
    error: false
  }
  
  const ua = (state = INITTIAL_STATE, action) => {
    if(action.type === 'LOAD_DATA_UA_REQUEST'){
      return {
        isFeching: true,
        data: [],
        error: false
      }
    }
    if(action.type === 'LOAD_DATA_UA_SUCCESS'){
      return {
        isFeching: true,
        data: action.data,
        error: false
      }
    }
    return state
  }
  export default ua